import unittest
from calculator import Calculator

class TestCalculatorMethods(unittest.TestCase):
    """Setup the Calculator object that can be used in the tests."""
    def setUp(self):
        self.calc = Calculator()

    def test_add(self):
        result = self.calc.add(2, 3)
        self.assertEqual(result, 5)

    def test_subtract(self):
        result = self.calc.subtract(5, 3)
        self.assertEqual(result, 2)

    def test_multiply(self):
        result = self.calc.multiply(2, 3)
        self.assertEqual(result, 6)

    def test_divide(self):
        result = self.calc.divide(6, 3)
        self.assertEqual(result, 2)

if _name_ == '_main_':
    unittest.main()