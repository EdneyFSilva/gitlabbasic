from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello":"Git 1st Exercise!"}
    
@app.get("/items/{item_id}")
def read_item{item_id: int, query_param: str - nome):
    return {"item_id": item_id, "query_param": query_param}
